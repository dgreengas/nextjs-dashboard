import { inter } from "@/app/ui/fonts";


export default function Page() {
    return <>
        <div className={`${inter.className}`}>Invoices</div>
    </>
}